# EnhancedPremake

## Introduction
**EnhancedPremake** is a module for [Premake5](https://premake.github.io/).
The solution built is only for C++17 project, with some specific settings. Have a look into the code to see what are exactly these settings.

## Using EnhancedPremake
Using **EnhancePremake** supposes that you already have some basics about Premake5.
Documentation for EnhancedPremake can be found here: [wiki](https://framagit.org/simnomce_u/enhanced_premake/wikis/home).

## Contributing to EnhancedPremake

Bugs can be reported on the Framagit issue tracker here: [![Open an issue](https://img.shields.io/badge/Framagit-Open%20an%20issue-blue.svg)](https://framagit.org/simnomce_u/enhanced_premake/issues)

## Authors
* IsilinBN
* Alcyone

## Copyright and Licensing
EnhancedPremake is delivered under the [GNU-GPLv3](https://www.gnu.org/licenses/gpl-3.0.fr.html).

EnhancedPremake is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
