# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/0.3.0/) and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [alpha-0.1.0 aka RTFM update] - 2017-05-14
### Added
- Setting a solution and all its projects.
- Adding premake option editing.
- Supporting external dependencies that also use EnhancedPremake API.
- Adding this changelog.
- Adding a readme.
- Providing that project to be used as a premake module.